package com.Lato;

public class Main {

    public static void main(String[] args) {
	    Dimensions dimensions = new Dimensions(30,20,10);
        Case theCase = new Case("220V", "Asus", "240", dimensions);

        Monitor monitor = new Monitor("27inch", "Dell", 27, new Resolution(2540,1440));

        Motherboard motherboard = new Motherboard("SJ-200", "Asus", 4, 2, "2.1");

        PC thePC = new PC(theCase, monitor, motherboard);
//        thePC.getMonitor().drawPixelAt(1500,200, "red");
//        thePC.getMotherboard().loadProgram("Windows 10");
//        thePC.getTheCase().pressPowerButton();

        thePC.powerUP();

    }
}
