public class NumberPalindrome {

    public static boolean isPalindrome(int number) {

        int savedNumber = number;
        int reverse = 0;

        while (number != 0) {
            int lastDigitOfNumber = number % 10;
            reverse *= 10; // add another place for next digit
            reverse += lastDigitOfNumber; // first of reversed digit
            number /= 10; // cut added to reverse digit
        }

        return (reverse == savedNumber) ? true : false;

    }

}
