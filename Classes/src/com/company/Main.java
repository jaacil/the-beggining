package com.company;

public class Main {

    public static void main(String[] args) {

        Car porsche = new Car(); //object of template Car called porsche
        Car holden = new Car();

        porsche.setModel("Carrera");
        System.out.println("Model is " + porsche.getModel());
    }
}
