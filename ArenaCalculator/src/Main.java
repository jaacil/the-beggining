public class Main {

    public static void main(String[] args) {
        double result = AreaCalculator.area(5.0);
        System.out.println("1 result: " + result);

        result = AreaCalculator.area(-1);
        System.out.println("2 result: " + result);

        result = AreaCalculator.area(5.0, 4.0);
        System.out.println("3 result: " + result);

        result = AreaCalculator.area(-1.0, 4.0);
        System.out.println("3 result: " + result);
    }

}
