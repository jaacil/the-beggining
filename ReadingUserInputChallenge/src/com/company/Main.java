package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int scanned = 0;

        for(int i = 0; i < 10; i++) {
            System.out.println("Enter number #" + (i + 1) + ": ");
            boolean hasNextInt = scanner.hasNextInt();
            if(hasNextInt) {
                scanned = scanner.nextInt();
                sum += scanned;
            } else {
                System.out.println("Invalid Value");
                i--;
            }
            scanner.nextLine(); //handle end of line (enter key)
        }
        System.out.println("Sum of entered numbers is: " + sum);
        scanner.close();
    }
}
