package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sum of those digits is " + sumDigits(125));
        System.out.println("Sum of those digits is " + sumDigits(-125));
        System.out.println("Sum of those digits is " + sumDigits(5));
        System.out.println("Sum of those digits is " + sumDigits(32123));
    }

    public static int sumDigits(int value) {
        if(value < 10) {
            return -1;
            }

        int sum = 0;
        while (value != 0) {

            int lastDigit = value % 10; // last digit from the value
            sum += lastDigit; // add this digit to sum

            value /=  10; // cut the last digit from value f.e. 125 % 10 = 5 -> 125 / 10 = 12 so 12 without 5 is 12, etc.

        }
    return sum;
    }

}
