package com.Lato;

public class Printer {

    private int tonerLevel;
    private int printedPages;
    private boolean duplexPrint;

    public Printer(int tonerLevel, boolean duplexPrint) {
        if(tonerLevel > -1 && tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = -1;
        }
        this.printedPages = 0; //because it self increment in method
        this.duplexPrint = duplexPrint;
    }


    public int getPrintedPages() {
        return printedPages;
    }


    public int fillToner(int tonerAmount) {
        if(tonerAmount <= 100 && tonerAmount > 0) {
            System.out.println("Toner level is: " + tonerLevel +"%");
            if((tonerAmount + tonerLevel) <= 100) {
                System.out.println("Filling with " + tonerAmount + "% of toner. Now toner capacity is: " + (this.tonerLevel += tonerAmount) + "%");
                return this.tonerLevel;
            } else {
                System.out.println("To much toner to fill up!");
                return -1;
            }
        } else {
            return -1;
        }
    }

    public int printPage(int pages) {
        System.out.println("Page printing");
        if(this.duplexPrint) {
            System.out.println("Printer is a duplex!");
            pages = (pages / 2) + (pages % 2);
        } else {
            System.out.println("Printer is not a duplex");
        }
        this.printedPages += pages;
        return pages;
    }


}
