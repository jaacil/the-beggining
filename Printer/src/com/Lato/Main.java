package com.Lato;

public class Main {

    public static void main(String[] args) {

	Printer printer = new Printer(20, false);

	System.out.println("Start page count: " + printer.getPrintedPages());

	System.out.println("Pages printed " + printer.printPage(4));
	System.out.println("Totally count of printed pages is: " + printer.getPrintedPages());

	printer.fillToner(50);


	printer.getPrintedPages();
	System.out.println(printer.getPrintedPages());

    }
}
