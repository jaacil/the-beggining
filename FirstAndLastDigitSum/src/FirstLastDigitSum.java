public class FirstLastDigitSum {

    public static int sumFirstAndLastDigit(int digit) {
        if(digit < 0) {
            return -1;
        }
        int lastDigit = digit % 10;
        int firstDigit = 0;

        while (digit != 0 ) {
            firstDigit = digit % 10;
            digit /= 10;
        }
        return lastDigit + firstDigit;
    }

}
