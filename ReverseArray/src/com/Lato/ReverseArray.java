package com.Lato;

import java.util.Arrays;

public class ReverseArray {

    public static void main(String[] args) {

        int[] myArray =  {1,5,3,7,11,9,15};

        reverse(myArray);


    }

    private static void reverse(int[] array) {

        System.out.println("Array = " + Arrays.toString(array));

        int[] copyOfArray = Arrays.copyOf(array, array.length);
        int temp = 0;
        int lengthOfArray = array.length - 1;

        for(int i = 0; i < array.length/2; i++) {

            temp = array[i];

            array[i] = copyOfArray[lengthOfArray];

            array[lengthOfArray] = temp;

            lengthOfArray -= 1;
        }
        System.out.println("Reversed array = " + Arrays.toString(array));

    }

}


