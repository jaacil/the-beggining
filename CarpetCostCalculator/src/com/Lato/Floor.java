package com.Lato;

public class Floor {

    private double width;
    private double length;

    public Floor(double width, double length) {
        if(width < 0 || length < 0) {
            if(width < 0) {
                this.width = 0;
            } else {
                this.width = width;
            }

            if(length < 0) {
                this.length = 0;
            } else {
                this.length = length;
            }
        } else {
            this.length = length;
            this.width = width;
        }
    }

    public double getArea() {
        return width * length;
    }


}
