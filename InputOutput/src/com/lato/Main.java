package com.lato;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    private static Locations locations = new Locations();

    private static Map<String, String> vocabulary = new HashMap<>();

    public static String command(String command) {

        String[] splitCommand = command.split(" ");

        if(command.length() == 1) {
            return command;
        }

        for(int i = 0; i < splitCommand.length; i++) {
//            System.out.println("split " + splitCommand[i]);
//            System.out.println("get " + vocabulary.get(splitCommand[i]));
            if(vocabulary.containsKey(splitCommand[i])) {
//                System.out.println("Returning : " + vocabulary.get(splitCommand[i]));
                return vocabulary.get(splitCommand[i]);
            }
        }
        return "There is no such destination!";
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        vocabulary.put("NORTH", "N");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("QUIT", "Q");




        //int loc = 1;
        int loc = 64;
        while (true) {

            System.out.println(locations.get(loc).getDescription());
//            tempExit.remove("S");
            if(loc == 0) {
                break;
            }

            Map<String, Integer> exits = locations.get(loc).getExits();
            System.out.print("Available exits are: ");

            for (String exit : exits.keySet()) {
                System.out.print(exit + ", ");
            }

            System.out.println();
            System.out.println("Where you want to go? ");
            String direction = scanner.nextLine().toUpperCase();
            //System.out.println(command(direction));
            String finalDirection = command(direction);

            if(exits.containsKey(finalDirection)) {
                loc = exits.get(finalDirection);
            } else {
                System.out.println("You cannot go in that direction");
            }
        }
    }
}
