public class SharedDigit {

    public static boolean hasSharedDigit (int firstValue, int secondValue) {
        if((firstValue < 10 || firstValue > 99) || (secondValue < 10 || secondValue > 99)) {
            return false;
        }

        int firstCondition = firstValue;
        int secondCondition = secondValue;

        int lastDigitOfFirst = 0;
        int firstDigitOfFirst = 0;
        int lastDigitOfSecond = 0;
        int firstDigitOfSecond = 0;

        while (firstCondition != 0) {
            lastDigitOfFirst = firstValue % 10;
            firstDigitOfFirst = firstValue / 10;
            firstCondition /= 10;
        }

        while (secondCondition != 0) {
            lastDigitOfSecond = secondValue % 10;
            firstDigitOfSecond = secondValue / 10;
            secondCondition /= 10;
        }

        if(lastDigitOfFirst == lastDigitOfSecond || firstDigitOfFirst == firstDigitOfSecond || lastDigitOfFirst == firstDigitOfSecond || lastDigitOfSecond == firstDigitOfFirst) {
            return true;
        }
        return false;
    }
}
