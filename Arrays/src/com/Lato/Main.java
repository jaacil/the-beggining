package com.Lato;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static int[] getIntegers(int number) {
        System.out.println("Enter " + number + " integer values");
        int[] values = new int[number];

        for(int i=0; i<values.length; i++) {
            values[i] = scanner.nextInt();
        }
        return values;
    }

    public static double getAverage(int[] array) {
        int sum = 0;
        for (int i=0; i<array.length; i++) {
            sum += array[i];
        }
        return (double) sum / (double) array.length;
    }

    public static void main(String[] args) {


        int[] myIntegers = getIntegers(5);
        printArray(myIntegers);
        System.out.println("Average is " + getAverage(myIntegers));



















	int[] myIntArrayVariable = new int[10];
//	int[] myIntArrayVariable = {1,2,3,4,5,6,7,8,9,10};
//	myIntArrayVariable[0] = 45;
//	myIntArrayVariable[5] = 50;

        for(int i=0; i<myIntArrayVariable.length; i++) {
            myIntArrayVariable[i] = i*10;
        }

        printArray(myIntArrayVariable);

	double[] myDoubleArray = new double[10];

        System.out.println(myIntArrayVariable[5]);

    }

    public static void printArray(int[] array){
        for(int i=0; i<array.length; i++){
            System.out.println("Element: " + i + ", value is: " + array[i]);
        }
    }

}
