package com.Lato;

public class Car extends Vehicle {

    private int wheels;
    private int doors;
    private int gears;
    private boolean isManual;

    private int currentGear;

    public Car(String type, String color, int engine, int wheels, int doors, int gears, boolean isManual, int currentGear) {
        super(type, color, engine, wheels);
        this.wheels = wheels;
        this.doors = doors;
        this.gears = gears;
        this.isManual = isManual;
        this.currentGear = currentGear;
    }

    public void changeGear(int currentGear) {
        this.currentGear = currentGear;
        System.out.println("Gear changed to " + this.currentGear);
    }

    public void changeSpeed(int speed, int direction) {
        System.out.println("Speed " + speed + " at direction " + direction);
        moving(speed, direction);
    }



}
