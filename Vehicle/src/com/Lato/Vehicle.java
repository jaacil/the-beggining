package com.Lato;

public class Vehicle {

    private String type;
    private String color;
    private int engine;


    private int currentSpeed;
    private int currentDirection;

    public Vehicle(String type, String color, int engine, int wheels) {
        this.type = type;
        this.color = color;
        this.engine = engine;

        this.currentDirection = 0;
        this.currentSpeed = 0;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEngine() {
        return engine;
    }

    public void setEngine(int engine) {
        this.engine = engine;
    }


    public void turning(int currentDirection) {
        this.currentDirection += currentDirection;
        System.out.println("Im turning: " + currentDirection);
    }

    public void moving(int speed, int direction) {
        currentSpeed = speed;
        currentDirection = direction;
        System.out.println("Im speeding by: " + currentSpeed + " at " + currentDirection + " direction");
    }

    public void stop() {
        this.currentSpeed = 0;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }
}
