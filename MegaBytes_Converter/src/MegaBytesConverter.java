public class MegaBytesConverter {

    public static void printMegaBytesAndKiloBytes(int kiloBytes) {
        if(kiloBytes < 0) {
            System.out.println("Invalid Value");
        } else {

            int reminder = kiloBytes % 1024;
            int value = kiloBytes / 1024;

            System.out.println(kiloBytes + " KB = " + value + " MB and " + reminder + " KB");

        }
    }
}
