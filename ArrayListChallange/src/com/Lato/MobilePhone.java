package com.Lato;

import java.util.ArrayList;

public class MobilePhone {

    private String myNumber;
    private ArrayList<Contact> myContacts;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContacts = new ArrayList<Contact>();
    }

    private int findContact(String name) {
        for(int i = 0; i < this.myContacts.size(); i++) {
            Contact contact = this.myContacts.get(i);
            if(contact.getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    private int findContact(Contact contact) {
        return this.myContacts.indexOf(contact);
    }

    public boolean addNewContact(Contact contact) {
        if(findContact(contact.getName()) >= 0) {
            System.out.println("Contact already exist");
            return false;
        } else {
            myContacts.add(contact);
            return true;
        }
    }

    public boolean updateContact(Contact oldContact, Contact newContact) {
        int position = findContact(oldContact);
        if(position >= 0 ) {
            myContacts.set(position, newContact);
            System.out.println(oldContact.getName() + " was replaced with " + newContact.getName());
            return true;
        } else if(findContact(newContact.getName()) != -1) {
            System.out.println("Contact with name " + newContact.getName() + " already exists. Update was not succesfull.");
            return false;
        } else {
            System.out.println(oldContact.getName() + " was not found.");
            return false;
        }
    }

    public boolean removeContact(Contact contactToRemove) {
        int position = findContact(contactToRemove);
        if(position >= 0) {
            this.myContacts.remove(contactToRemove);
            System.out.println(contactToRemove.getName() + " was deleted.");
            return true;
        } else {
            System.out.println(contactToRemove.getName() + " was not found.");
            return false;
        }
    }

    public String queryContact(Contact contact) {
        if(findContact(contact) >= 0) {
            return contact.getName();
        } else {
            return null;
        }
    }

    public Contact queryContact(String name) {
        int position = findContact(name);
        if(position >= 0) {
            return this.myContacts.get(position);
        }
        return null;
    }

    public void printContacts() {
        System.out.println("Contact list: ");
        for(int i = 0; i < myContacts.size(); i++) {
            System.out.println((i+1) + ". " + this.myContacts.get(i).getName() + " -> " + this.myContacts.get(i).getPhoneNumber());
        }
    }


}
