package com.company;

public class Main {

    public static void main(String[] args) {

//        int value = 1;
//        if(value == 1) {
//            System.out.println("Value was 1");
//        } else if(value == 2) {
//            System.out.println("Value was 2");
//            } else {
//            System.out.println("Was not 1 or 2");
//        }

        int switchValue = 1;

        switch (switchValue) {
            case 1:
                System.out.println("Value was 1");
                break;

            case 2:
                System.out.println("Value was 2");
                break;

            case 3: case 4: case 5:
                System.out.println("Was 3 or 4 or 5");
                System.out.println("Actually it was " + switchValue);
                break;

            default:
                System.out.println("Was not 1,2,3,4 or 5");
                break;
        }

        char switchValueChar = 'C';
        switch (switchValueChar) {
            case 'A':
                System.out.println("A found!");
                break;
            case 'B':
                System.out.println("B found!");
                break;
            case 'C': case 'D': case 'E':
                System.out.println(switchValueChar + " found!");
                break;
            default:
                System.out.println("No A, B, C, D or E found!");
        }

        String month = "jANUary";
        switch(month.toLowerCase()) {
            case "january":
                System.out.println("Jan");
                break;
            case "june":
                System.out.println("Jun");
                break;
            default:
                System.out.println("Not suer");
        }


        // more code here

    }
}
