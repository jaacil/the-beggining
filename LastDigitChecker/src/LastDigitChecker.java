public class LastDigitChecker {

    public static boolean hasSameLastDigit(int firstValue, int secondValue, int thirdValue) {
        if((firstValue < 10 || firstValue > 1000) || (secondValue < 10 || secondValue > 1000) || (thirdValue < 10 || thirdValue > 1000)) {
            return false;
        }
//Tu petla nie potrzeba bo sprawdzamy tylko jedna liczbe - z konca!

//        int firstCondition = firstValue;
//        int secondCondition = secondValue;
//        int thirdCondition = thirdValue;

        int lastDigitOfFirst = 0;
        int lastDigitOfSecond = 0;
        int lastDigitOfThird = 0;

        lastDigitOfFirst = firstValue % 10;
        lastDigitOfSecond = secondValue % 10;
        lastDigitOfThird = thirdValue % 10;

//        while(firstCondition != 0) {
//            lastDigitOfFirst = firstValue % 10;
//            firstCondition /= 10;
//        }
//
//        while(secondCondition != 0) {
//            lastDigitOfSecond = secondValue % 10;
//            secondCondition /= 10;
//        }
//
//        while(thirdCondition != 0) {
//            lastDigitOfThird = thirdValue % 10;
//            thirdCondition /= 10;
//        }

        if((lastDigitOfFirst == lastDigitOfSecond) || (lastDigitOfSecond == lastDigitOfThird) || (lastDigitOfThird == lastDigitOfFirst)) {
            return true;
        }
        return false;
    }

    public static boolean isValid(int value) {
        if(value < 10 || value > 1000) {
            return false;
        }
        return true;
    }

}
