package com.Lato;

import java.util.ArrayList;

class IntClass { //wrapper
    private int myValue;

    public IntClass(int myValue) {
        this.myValue = myValue;
    }

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue(int myValue) {
        this.myValue = myValue;
    }
}


public class Main {

    public static void main(String[] args) {
	String[] strArray = new String[10];
	int[] intArray = new int[10];

        ArrayList<String> strArrayList= new ArrayList<String>();
        strArrayList.add("Jack");

        ArrayList<IntClass> intClassArrayList = new ArrayList<IntClass>();
        intClassArrayList.add(new IntClass(54)); //from wrapper class

        // LONG WAY
        Integer integer = new Integer(54); // new object of Integer class
        Double doubleValue = new Double(12.25);

        ArrayList<Integer> intArrayList = new ArrayList<Integer>(); // its ok, because Integer is a class
        for (int i = 0; i <= 10; i++) {
            intArrayList.add(Integer.valueOf(i)); //auto boxing
        }

        for (int i = 0; i <= 10; i++) {
            System.out.println(i + " --> " + intArrayList.get(i).intValue()); //unboxing
        }

        //SHOR WAY
        Integer myIntValue = 56; // -> this is executed ... = Integer.valueOf(56);
        int myInt = myIntValue; // -> this is executed ... = myIntValue.intValue();







        ArrayList<Double> myDoubleValues = new ArrayList<Double>();

        for(double dbl = 0.0; dbl<=10.0; dbl += 0.5) {
            myDoubleValues.add(Double.valueOf(dbl)); //auto boxing // it can be like this : myDoubleValues.add(dbl);
        }

        for(int i = 0; i<myDoubleValues.size(); i++) {
            System.out.println(i + " -> " + myDoubleValues.get(i).doubleValue()); //unboxing // it can be like this : myDoubleValues.get(i); //java is automatically add commands
        }

    }
}
