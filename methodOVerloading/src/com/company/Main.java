package com.company;

public class Main {

    public static void main(String[] args) {
//	int newScore = calculateStore("Jacek", 500);
//        System.out.println("New score " + newScore);
//        calculateStore(75);
//        calculateStore();

        double centimeters = calcFeetAndInchesToCentimeters(6, 0);
        System.out.println("Firs method - centimeters " + centimeters);
        if(centimeters<0d){
            System.out.println("Invalid parameters");
        }

        calcFeetAndInchesToCentimeters(156);

    }

    public static int calculateStore(String playerName, int score) {
        System.out.println("Player " + playerName + " scored " + score + " points.");
        return score * 1000;
    }

    public static int calculateStore(int score) {
        System.out.println("Unnamed player scored " + score + " points.");
        return score * 1000;
    }

    public static int calculateStore() {
        System.out.println("No player name, no player score");
        return 0;
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if((feet >= 0) && (inches >= 0 && inches <= 12)) {
            double centimeters = (feet * 30.48d) + (inches * 2.54d);
            System.out.println(feet + " feet, " + inches + " inches = " + centimeters + " cm.");
            return  centimeters;
        } else {
            System.out.println("Invalid feet or inches!");
            return -1;
        }
    }

    public  static double calcFeetAndInchesToCentimeters(double inches) {
        if(inches >= 0) {
            double feet = (int)inches / 12;
            double reminderInches = (int)inches % 12;
            System.out.println(inches + " inches is equal to " + feet + " feet and " + reminderInches + " inches. ");
            return calcFeetAndInchesToCentimeters(feet, reminderInches);
        } else return -1;
    }

}
