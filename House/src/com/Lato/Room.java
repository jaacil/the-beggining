package com.Lato;

public class Room {

    private Window theWindow;
    private Door theDoor;
    private Furniture theFurniture;
    private Dimensions theDimensions;

    public Room(Window theWindow, Door theDoor, Furniture theFurniture, Dimensions theDimensions) {
        this.theWindow = theWindow;
        this.theDoor = theDoor;
        this.theFurniture = theFurniture;
        this.theDimensions = theDimensions;
    }

    public void createRoom() {
        theDoor.lockTheDoor(1);
        closeWindows();
    }

    private void closeWindows() {
        theWindow.windowOpened(0);
    }

    public Window getTheWindow() {
        return theWindow;
    }

    public Door getTheDoor() {
        return theDoor;
    }

    public Furniture getTheFurniture() {
        return theFurniture;
    }

    public Dimensions getTheDimensions() {
        return theDimensions;
    }
}
