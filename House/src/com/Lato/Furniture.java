package com.Lato;

public class Furniture {

    private String producer;
    private String material;
    private Dimensions dimensions;

    public Furniture(String producer, String material, Dimensions dimensions) {
        this.producer = producer;
        this.material = material;
        this.dimensions = dimensions;
    }

    public String getProducer() {
        return producer;
    }

    public String getMaterial() {
        return material;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }
}
