package com.Lato;

public class Window {

    private String color;
    private int layersOfGlass;
    private double sunTransmittance;
    private Dimensions dimensions;

    public Window(String color, int layersOfGlass, double sunTransmittance, Dimensions dimensions) {
        this.color = color;
        this.layersOfGlass = layersOfGlass;
        this.sunTransmittance = sunTransmittance;
        this.dimensions = dimensions;
    }

    public void windowOpened(int angle) {
        System.out.println("Window height: " + dimensions.getHeight());
        openWindow(angle);
    }

    private void openWindow(int angle) {
        System.out.println("Window open at " + angle + " angle.");
    }
}
