package com.Lato;

public class Door {

    private boolean isAntiBurglary;
    private String color;
    private int locks;
    private boolean isLocked;
    private Dimensions dimensions;

    public Door(boolean isAntiBurglary, String color, int locks, Dimensions dimensions) {
        this.isAntiBurglary = isAntiBurglary;
        this.color = color;
        this.locks = locks;
        this.dimensions = dimensions;
    }

    public void openDoors(int angle) {
        if(!isLocked) {
            System.out.println("Open doors at: " + angle + " degrees.");
        }
    }

    public void lockTheDoor(int locks) {
        if(!isLocked) {
            if(locks <= this.locks) {
                System.out.println("Doors were locked using " + locks + " locks.");
            } else {
                System.out.println("Doors don't have so many locks!");
            }
        } else {
            System.out.println("Doors are already locked!");
        }
    }

    public boolean isAntiBurglary() {
        return isAntiBurglary;
    }

    public String getColor() {
        return color;
    }

    public int getLocks() {
        return locks;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }
}
