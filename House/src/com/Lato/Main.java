package com.Lato;

public class Main {

    public static void main(String[] args) {

        Dimensions windowD = new Dimensions(120, 60, 5);
        Window window = new Window("White", 2, 120, windowD);

        Dimensions doorsD = new Dimensions(200, 80, 10);
        Door doors = new Door(true, "Brown", 2, doorsD);

        Dimensions furnitureD = new Dimensions(500, 140, 50);
        Furniture furniture = new Furniture("SuperWood", "Wood", furnitureD);

        Dimensions roomD = new Dimensions(520, 240, 250);
        Room room = new Room(window, doors, furniture, roomD);

        room.createRoom();

        window.windowOpened(15);
        doors.lockTheDoor(2);

        
        System.out.println(room.getTheDoor().isAntiBurglary());

    }
}
