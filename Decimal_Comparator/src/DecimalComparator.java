public class DecimalComparator {

    public static boolean areEqualByThreeDecimalPlaces(double firstDecimal, double secondDecimal) {
        int intFirstDecimal = (int)(firstDecimal * 1000); //* 1000 przesuwa przecinek w prawo o 3 miejsca wiec sprawdza to 3 znaki po przecinku np 3.123 = 3123 itd.
        System.out.println(intFirstDecimal);
        int intSecondDecimal = (int)(secondDecimal * 1000);
        System.out.println(intSecondDecimal);
        if (intFirstDecimal == intSecondDecimal) {
            return true;
        } else {
            return false;
        }
    }

}
