public class sandm_challenge {

    private static final String INVALID_VALUE_MESSAGE = "Invalid value";

    public static String getDurationString(int minutes, int seconds) {

        if((minutes < 0) || (seconds < 0) || (seconds > 59 )) {
            return INVALID_VALUE_MESSAGE;
        }
            int hours = minutes / 60;
            int reminderOfHours = minutes % 60;

            String hoursString = hours + "h";
            String minutesString = reminderOfHours + "m";
            String secondsString = seconds + "s";
            if(hours < 10) {
                hoursString = "0" + hoursString;
            }
            if(reminderOfHours < 10) {
                minutesString = "0" + minutesString;
            }
            if(seconds < 10) {
                secondsString = "0" + secondsString;
        }

            return hoursString + " " + minutesString + " " + secondsString;
    }

    public static String getDurationString(int seconds) {
        if(seconds < 0) {
            return INVALID_VALUE_MESSAGE;
        }
        int minutes = seconds / 60;
        int reminderOfSeconds = seconds % 60;
//        System.out.println(getDurationString(minutes, reminderOfSeconds));
//        return minutes + "m " + reminderOfSeconds + "s ";
        return getDurationString(minutes, reminderOfSeconds);
    }

}
