package com.Lato;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

	int myIntValue = 10; //obiekt
	int anotherIntValue = myIntValue; //inny obiekt

        System.out.println("myIntValue = " + myIntValue);
        System.out.println("anotherIntValue = " + anotherIntValue);

        anotherIntValue++;

        System.out.println("myIntValue = " + myIntValue);
        System.out.println("anotherIntValue = " + anotherIntValue);

        int[] myIntArray = new int[5]; //adres obiektu a nie obiekt!! - to referencaja do tablicy w pamieci
        int[] anotherArray = myIntArray; //kolejna referencja wskazujaca tą samą tablicę w pamięci

        System.out.println("myIntArray= " + Arrays.toString(myIntArray));
        System.out.println("anotherArray= " + Arrays.toString(anotherArray));

        anotherArray[0] = 1; //zmienia jedna i druga bo wskazuja na to samo miejsce w pamieci!

        System.out.println("After change myIntArray= " + Arrays.toString(myIntArray));
        System.out.println("After change anotherArray= " + Arrays.toString(anotherArray));

        anotherArray = new int[] {4,5,6,7,8};

        modifyArray(myIntArray);

        System.out.println("After modify myIntArray= " + Arrays.toString(myIntArray));
        System.out.println("After modify anotherArray= " + Arrays.toString(anotherArray));

    }


    private static void modifyArray(int[] array) { //parametr to kolejna referencja
        array[0] = 2;
        array = new int[] {1,2,3,4,5};
    }
}
