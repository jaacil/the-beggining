package com.Lato;

import java.util.*;

public class Album {

    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }

    private Song findSong(String titleOfSong) {

        for(Song checkSong: this.songs) { //for each Song checkSong - check if there is the same name what is in parameter
            if(checkSong.getTitle().equals(titleOfSong)) {
                return checkSong;
            }
        }
        //long version
//        for(int i = 0; i < songs.size(); i++) {
//            Song song = this.songs.get(i);
//
//            if(song.getTitle().equals(titleOfSong)) {
//                System.out.println("Returning song");
//                return song;
//            }
//        }
        return null;
    }

    public boolean addSong(String titleOfSong, double duration) {
        if(findSong(titleOfSong) == null) {
            Song newSong = new Song(titleOfSong, duration);
            this.songs.add(newSong);
            return true;
        } else {
            return false;
        }
    }

    public boolean addToPlayList(int trackNo, List<Song> playList) {
        int index = trackNo - 1;
        if((index >= 0) && (index <= this.songs.size())) {
            playList.add(this.songs.get(index));
            return true;
        }
        System.out.println("This album does not have such a track " + trackNo);
        return false;
    }

    public boolean addToPlayList(String titleOfSong, List<Song> playList) {
        Song song = findSong(titleOfSong);
        if(song != null) {
            playList.add(song);
            return true;
        }
        System.out.println("This song " + titleOfSong + " is not in this album");
        return false;
    }



}
