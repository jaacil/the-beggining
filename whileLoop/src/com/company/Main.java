package com.company;

public class Main {

    public static void main(String[] args) {
//	int count = 1;
//	while (count != 6) {
//        System.out.println("Count Value is " + count);
//        count++;
//    }
//    count =1;
//	do {
//        System.out.println("Count value was " + count);
//        count++;
//    } while(count !=6);

    int number = 4;
    int finishNumber = 20;
    int counter = 0;

    while (number <= finishNumber) {
        number++;
        if(!isEvenNumber(number)) {
            continue; //if number is not evenNumber - bypass any code further down, it goes straight to end of the loop and then back to start.
            //continue with another condition
        }
        counter++;
        System.out.println("Even number " + number); // this code is executed and then go to condition, while number <= finishNumber

        if(counter == 5) {
            break;
        }
    }
        System.out.println("Counter is: " + counter);


    }

    public static boolean isEvenNumber(int value) {
        if(value % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }


}
