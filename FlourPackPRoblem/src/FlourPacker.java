public class FlourPacker {

    public static boolean canPack(int bigCount, int smallCount, int goal) {

        if (bigCount < 0 || smallCount < 0 || goal < 0) {
            return false;
        }

        bigCount *= 5;
        smallCount *= 1;

        if (bigCount + smallCount < goal) {
            return false;
        }

        if (goal % 5 <= smallCount) {
            return true;
        }
        return false;
    }
}
