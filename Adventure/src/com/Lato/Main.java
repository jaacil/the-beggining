package com.Lato;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    private static Map<Integer, Location> locations = new HashMap<>();

    private static Map<String, String> vocabulary = new HashMap<>();

    public static String command(String command) {

        String[] splitCommand = command.split(" ");

        if(command.length() == 1) {
            return command;
        }

        for(int i = 0; i < splitCommand.length; i++) {
//            System.out.println("split " + splitCommand[i]);
//            System.out.println("get " + vocabulary.get(splitCommand[i]));
            if(vocabulary.containsKey(splitCommand[i])) {
//                System.out.println("Returning : " + vocabulary.get(splitCommand[i]));
                return vocabulary.get(splitCommand[i]);
            }
        }
        return "There is no such destination!";
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        vocabulary.put("NORTH", "N");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("QUIT", "Q");

        Map<String, Integer> tempExit = new HashMap<>();
        locations.put(0, new Location(0, "You are sitting in front of a computer learning Java", tempExit));

        tempExit = new HashMap<>();
        tempExit.put("W", 2);
        tempExit.put("E", 3);
        tempExit.put("S", 4);
        tempExit.put("N", 5);
        locations.put(1, new Location(1, "You are standing at the end of a road before a small brick building",tempExit));


        tempExit = new HashMap<>();
        tempExit.put("N", 5);
        locations.put(2, new Location(2, "You are at the top of a hill",tempExit));


        tempExit = new HashMap<>();
        tempExit.put("W", 1);
        locations.put(3, new Location(3, "You are inside a building, a well house for a small spring",tempExit));


        tempExit = new HashMap<>();
        tempExit.put("N", 1);
        tempExit.put("W", 2);
        locations.put(4, new Location(4, "You are in a valley beside a stream",tempExit));


        tempExit = new HashMap<>();
        tempExit.put("S", 1);
        tempExit.put("W", 2);
        locations.put(5, new Location(5, "You are in the forest",tempExit));


        int loc = 1;
        while (true) {

            System.out.println(locations.get(loc).getDescription());
//            tempExit.remove("S");
            if(loc == 0) {
                break;
            }

            Map<String, Integer> exits = locations.get(loc).getExits();
            System.out.print("Available exits are: ");

            for (String exit : exits.keySet()) {
                System.out.print(exit + ", ");
            }

            System.out.println();
            System.out.println("Where you want to go? ");
            String direction = scanner.nextLine().toUpperCase();
            //System.out.println(command(direction));
            String finalDirection = command(direction);

            if(exits.containsKey(finalDirection)) {
                loc = exits.get(finalDirection);
            } else {
                System.out.println("You cannot go in that direction");
            }
        }
    }
}
