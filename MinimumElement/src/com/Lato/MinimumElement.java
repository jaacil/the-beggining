package com.Lato;

import java.util.Arrays;
import java.util.Scanner;

public class MinimumElement {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = readElements(readInteger());
        System.out.println("Min number is: " + findMin(array));

    }

    private static int readInteger() {

        System.out.println("How many elements do you want to enter?");
        int numberOfElements = scanner.nextInt();
        scanner.nextLine();
        return numberOfElements;
    }

    private static int[] readElements(int number) {
        int[] array = new int[number];
        for(int i = 0; i < array.length; i++){
            System.out.println("Type number " + (i+1) + " from " + array.length);
            array[i] = scanner.nextInt();
            scanner.nextLine();
        }
        System.out.println("Your array: " + Arrays.toString(array));
        return array;
    }

    private static int findMin(int[] array) {
        int minValue = Integer.MAX_VALUE;

            for (int i = 0; i < array.length; i++) {
                if (array[i] < minValue) {
                    minValue = array[i];
                }
            }

        return minValue;
    }

}
