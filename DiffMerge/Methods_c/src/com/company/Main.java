package com.company;

public class Main {

    public static void main(String[] args) {
        boolean gameOver = true;
        int score = 800;
        int levelCompleted = 5;
        int bonus = 100;

        int highScore = calculateScore(gameOver, score, levelCompleted, bonus);
        System.out.println("Your final score is: " + highScore);

        score = 10000;
        levelCompleted = 8;
        bonus = 200;

        highScore = calculateScore(gameOver, score, levelCompleted, bonus);
        System.out.println("Your final score is: " + highScore);

        int challenge2IntReturn = calculatedHighScorePosition(1500);
        displayHighScorePosition("Jacek", challenge2IntReturn);

        challenge2IntReturn = calculatedHighScorePosition(900);
        displayHighScorePosition("Tomek", challenge2IntReturn);

        challenge2IntReturn = calculatedHighScorePosition(400);
        displayHighScorePosition("Sasha", challenge2IntReturn);

        challenge2IntReturn = calculatedHighScorePosition(50);
        displayHighScorePosition("Mika", challenge2IntReturn);

    }
    public static int calculateScore(boolean gameOver, int score, int levelCompleted, int bonus) {

        if (gameOver) {
            int finalScore = score + (levelCompleted * bonus);
            finalScore += 2000;
            return finalScore;
        }
        return -1;
    }

    public static void displayHighScorePosition(String playerName, int position) {
        System.out.println(playerName + " managed to get position " + position + " on the high score table");
    }

    public static int calculatedHighScorePosition(int playerScore) {
//        if (playerScore >= 1000) {
//            return 1;
//        } else if (playerScore >= 500) {
//            return 2;
//        } else if (playerScore >= 100) {
//            return 3;
//        }
//        return 4;
        int newPosition = 4; //assuming that position will be returned)
        if(playerScore >= 1000){
            newPosition = 1;
        } else if(playerScore >=500) {
            newPosition = 2;
        } else if(playerScore >=100) {
            newPosition = 3;
        }

        return newPosition;
    }
}
