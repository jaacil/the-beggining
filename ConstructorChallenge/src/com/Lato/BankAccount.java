package com.Lato;

public class BankAccount {

    private String accountNumber;
    private double balance;
    private String customerName;
    private String email;
    private String phone;

    public BankAccount() {
        this("56789", 2.50, "Default name", "Default email", "Default phone"); //if empty constructor - create a default
        // object with those parameters (optional) - if u want to create a object with all parameters set - must be first statement
        System.out.println("Empty constructor called");
    }

    public BankAccount(String accountNumber, double balance, String customerName, String email, String phone) {
        System.out.println("Constructor with parameters");
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phone = phone;
    }

    public BankAccount(String customerName, String email, String phone) {
        this("99999", 100.55, customerName, email, phone); //constructor with 3 parameters and two defaults
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
    public double getBalance() {
        return balance;
    }
    public String getCustomerName() {
        return customerName;
    }
    public String getEmail() {
        return email;
    }
    public String getPhone() {
        return phone;
    }

    public void setDepositFunds(double depositAmount) {
        this.balance += depositAmount;
        System.out.println("Deposit of " + depositAmount + " succeed. New balance is: " + this.balance);
    }
    public void setWithdraw(double withdrawAmount) {
        if(this.balance < withdrawAmount) {
            System.out.println("You have insufficient funds!");
        } else {
            this.balance -= withdrawAmount;
            System.out.println("Withdrawal of " + withdrawAmount + " succeed. Remaining balance is: " + this.balance);
        }
    }

}
