package com.Lato;


public class Main {

    public static void main(String[] args) {
	BankAccount bankAccount = new BankAccount("12345", 25000, "Jacek Lato", "jac_jac@email.com", "123456789");
    //constructor with parrameters
//	bankAccount.setAccountNumber("1234432145679876");
//	bankAccount.setBalance(25000.0);
//	bankAccount.setCustomerName("Jacek Lato");
//	bankAccount.setEmail("jack_jack@email.com");
//	bankAccount.setPhone("123456789");

//        System.out.println("Account number is: " + bankAccount.getAccountNumber());
//        System.out.println("Customer names is: " + bankAccount.getCustomerName());
//        System.out.println("Customer email and phone are: " + bankAccount.getEmail() + "; " + bankAccount.getPhone());
//        System.out.println("Current balance is: " + bankAccount.getBalance());
//
//        bankAccount.setDepositFunds(25000);
////        System.out.println("Current balance is: " + bankAccount.getBalance());
//
//        bankAccount.setWithdraw(49000);
////        System.out.println("Current balance is: " + bankAccount.getBalance());
//
//        bankAccount.setWithdraw(2000);
////        System.out.println("Current balance is: " + bankAccount.getBalance());
//
//        BankAccount jackAccount = new BankAccount("Jacek" , "Jacek_jac@email.com", "12345");
//        System.out.println(jackAccount.getAccountNumber() + " name " + jackAccount.getCustomerName());

        VipCustomer customer1 = new VipCustomer();
        System.out.println(customer1.getName());

        VipCustomer customer2 = new VipCustomer("Jim", 25000);
        System.out.println(customer2.getName());

        VipCustomer customer3 = new VipCustomer("Jane" , 100, "jane@email.com");
        System.out.println(customer3.getName());
        System.out.println(customer3.getEmailAddress());

    }
}
