package com.Lato;

public class Main {

    public static void main(String[] args) {
	ITelephone jackPhone;
	jackPhone = new DeskPhone(123456789);

	jackPhone.powerOn();
	jackPhone.callPhone(123456789);
	jackPhone.answer();

	jackPhone = new MobilePhone(987654321);
	jackPhone.powerOn();
	jackPhone.callPhone(987654321);
	jackPhone.answer();

    }
}
