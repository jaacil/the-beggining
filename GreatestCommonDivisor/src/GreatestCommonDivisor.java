public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int first, int second) {
        if(first < 10 || second < 10) {
            return -1;
        }

        int divider = 0;

        for(int i = Math.min(first, second); i != 1 ; i--) {
            if((first % i == 0) && (second % i == 0)) {
                divider = i;
                break;
            }
        }
        return divider;
    }
}
