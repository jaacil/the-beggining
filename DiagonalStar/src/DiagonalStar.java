public class DiagonalStar {

    public static void printSquareStar(int number) {
        if (number < 5) {
            System.out.println("Invalid Value");
        } else {

            int rowAndColumnCount = number;
            int currentRow = 0;
            int currentColumn = 0;

            for (int rows = number; rows >= 1; rows--) {

                currentRow = rowAndColumnCount - rows + 1;

                for (int column = number; column >= 1; column--) {

                    currentColumn = rowAndColumnCount - column + 1;

                    if (currentColumn == 1 || currentColumn == number) {
                        System.out.print("*"); //ok
                    } else if (currentRow == 1 || currentRow == number) {
                        System.out.print("*"); //ok
                    } else if (currentColumn == rowAndColumnCount - currentRow + 1) {
                        System.out.print("*");
                    } else if (currentRow == currentColumn) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }

        }
    }

}
