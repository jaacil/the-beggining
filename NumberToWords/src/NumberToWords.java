public class NumberToWords {

    public static void numberToWords(int number) {
        if(number < 0) {
            System.out.println("Invalid Value");
        }

        int reversedNumber = reverse(number);

            for(int i = 0; i < getDigitCount(number); i++) {
                int lastDigit = reversedNumber % 10;
                reversedNumber /= 10;

                switch (lastDigit) {
                    case 0:
                        System.out.println("Zero");
                        break;
                    case 1:
                        System.out.println("One");
                        break;
                    case 2:
                        System.out.println("Two");
                        break;
                    case 3:
                        System.out.println("Three");
                        break;
                    case 4:
                        System.out.println("Four");
                        break;
                    case 5:
                        System.out.println("Five");
                        break;
                    case 6:
                        System.out.println("Six");
                        break;
                    case 7:
                        System.out.println("Seven");
                        break;
                    case 8:
                        System.out.println("Eight");
                        break;
                    case 9:
                        System.out.println("Nine");
                        break;
                    default:
                        System.out.println("Invalid Value");
                        break;
                }
            }
    }


    public static int reverse(int number) {

        int reversed = 0;
        while(number != 0) {
            int lastDigit = number % 10;
            reversed *= 10;
            reversed += lastDigit;
            number /= 10;
        }
        return reversed;
    }

    public static int getDigitCount(int number) {
        if(number < 0) {
            return -1;
        }
        int counter = 0;
        if(number == 0) {
            counter = 1;
        } else {
            while (number != 0) {
                number /= 10;
                counter++;
            }
        }
        return counter;
    }



}
