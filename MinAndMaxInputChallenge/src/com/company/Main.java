package com.company;

import java.util.Enumeration;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int min = 0;
        int max = 0;
        boolean first = true;

        while(true) {
            System.out.println("Enter a number: ");
            boolean hasNextInt = scanner.hasNextInt();
            if(hasNextInt) {
                int scanned = scanner.nextInt();

                if(first) {
                    first = false;
                    min = scanned;
                    max = scanned;
                }

                if(scanned > max) {
                    max = scanned;
                } else if(scanned < min) {
                    min = scanned;
                }
            } else {
                System.out.println("Maximum number is: " + max + ", and the minimum number is: " + min);
                break;
            }
            scanner.nextLine(); //handle
        }

        scanner.close();

    }
}
