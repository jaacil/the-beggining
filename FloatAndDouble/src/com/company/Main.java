package com.company;

import java.time.zone.ZoneOffsetTransition;

public class Main {

    public static void main(String[] args) {
	    float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;
        System.out.println("Float minimum Value = " + myMinFloatValue);
        System.out.println("Float maximum value = " + myMaxFloatValue);

        double myMinDoubleValue = Double.MIN_VALUE;
        double myMaxDoubleValue = Double.MAX_VALUE;
        System.out.println("Double minimum Value = " + myMinDoubleValue);
        System.out.println("Double maximum value = " + myMaxDoubleValue);

        int myIntValue = 5 / 3;
        float myFloatValue = 5f / 3f;
        double myDoubleValue = 5.00 / 3.00;
        System.out.println("MyIntValue = " + myIntValue);
        System.out.println("MyFloatValue = " + myFloatValue);
        System.out.println("MyDoubleValue = " + myDoubleValue);

        double myPoundsValue = 200;
        double onePound = 0.45359237;
        double myResult = myPoundsValue * onePound;
        System.out.println("Pounds converted to kilograms = " + myResult);

        double pi = 3.1415927d;
        double anotherNumber = 3_000_000.4_567_890;
        System.out.println(pi);
        System.out.println(anotherNumber);
    }
}
