package com.company;

public class Main {

    public static void main(String[] args) {
	double firstDouble = 20.00d;
	double secondDouble = 80.00d;
	double addAndMultiply = (firstDouble + secondDouble) * 100.00;

	double reminderOfStep3 = addAndMultiply % 40.00d;

	boolean step5Check = (reminderOfStep3 == 0) ? true : false;

        System.out.println("The logical result of step 5 is " + step5Check);

        if (!step5Check){
            System.out.println("Got some reminder");
        }

    }
}
