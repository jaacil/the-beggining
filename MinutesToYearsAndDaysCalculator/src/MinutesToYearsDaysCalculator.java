public class MinutesToYearsDaysCalculator {

    public static void printYearsAndDays(long minutes) {
        if(minutes < 0) {
            System.out.println("Invalid Value");
        } else {
            int hour = (int) minutes / 60;
            int day = hour / 24;
            int year = day / 365;
            int restOfYears = day % 365;

            System.out.println(minutes + " min = " + year + " y and " + restOfYears + " d");
        }
    }

}
